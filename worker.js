const Queue = require('bull');

const REDIS_URL = process.env.REDIS_URL || 'redis://0.0.0.0:6379';

const fetchUserQueue = new Queue('fetch-user', REDIS_URL);

const maxJobsPerWorker = 1;

fetchUserQueue.process(maxJobsPerWorker, async (job) => {
  const username = job.data.username;

  const timestamp = new Date().getTime();
  console.log(`fetching user ${username}`);
});

console.log('Worker running');
