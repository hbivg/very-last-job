const form = document.querySelector('form');

function getUsernames() {
  const textareaValue = document.getElementById('usernames').value;

  const lines = textareaValue.split('\n');
  const trimmedLines = lines.map((line) => line.trim().replace(/ /g, ''));
  const nonEmptyLines = trimmedLines.filter((line) => line.length > 0);

  return nonEmptyLines;
}

async function sendUsernamesToFetch(usernames) {
  const response = await fetch('/fetch-users', {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({ usernames }),
  });
  const json = await response.json();

  if (json.status !== 'success') {
    throw new Error(json.error);
  }
}

function appendMessageToBody(message) {
  const messageElement = document.createElement('p');
  messageElement.textContent = message;
  document.body.appendChild(messageElement);
}

async function submitForm(event) {
  event.preventDefault();
  
  const usernames = getUsernames();

  try {
    await sendUsernamesToFetch(usernames);
  } catch (error) {
    const message = `Failed sending usernames to fetch (error: ${error.message})`;
    appendMessageToBody(message);
  }
}

form.addEventListener('submit', submitForm);

const socket = io();

socket.onAny((eventName, message) => {
  appendMessageToBody(`${eventName} ${message}`);
});
