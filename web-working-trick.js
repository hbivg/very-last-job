const express = require('express');
const http = require('http');
const path = require('path');
const Queue = require('bull');
const { Server } = require('socket.io');

const PORT = process.env.PORT || 3000;
const REDIS_URL = process.env.REDIS_URL || 'redis://0.0.0.0:6379';

const fetchUserQueue = new Queue('fetch-user', REDIS_URL);

const app = express();

app.use(express.urlencoded({ extended: true }));
app.use(express.json());

const server = http.createServer(app);
const io = new Server(server);

app.use(express.static('public'));

app.get('/', (req, res) => {
  res.sendFile(path.resolve(__dirname, 'public/index.html'));
});

let alreadyGotLastJob = false;

app.post('/fetch-users', async (req, res) => {
  alreadyGotLastJob = false;

  try {
    const usernames = req.body.usernames;

    for (let i = 0; i < usernames.length; i += 1) {
      const username = usernames[i];
      await fetchUserQueue.add({ username });
    }

    res.json({ status: 'success' });
  } catch (error) {
    res.json({ status: 'failure', error: error.message });
  }
});

io.on('connection', () => {
  console.log('web socket.io client connected');
});

fetchUserQueue.on('global:error', (error) => {
  const event = 'queue error "fetch-user"';
  console.log(`${event} ${error}`);
  io.emit(event, error);
});

fetchUserQueue.on('global:failed', (jobId, error) => {
  const event = 'queue failed "fetch-user"';
  console.log(`${event} ${jobId} ${error}`);
  io.emit(event, `${jobId} ${error}`);
});

fetchUserQueue.on('global:completed', async (jobId) => {
  const timestamp = new Date().getTime();
  const event = 'queue completed "fetch-user"';

  const job = await fetchUserQueue.getJob(jobId);
  const username = job.data.username;

  console.log(`${event} ${timestamp} ${jobId} ${username}`);
  io.emit(event, `${timestamp} ${jobId} ${username}`);

  const jobCounts = await fetchUserQueue.getJobCounts();
  const t = new Date().getTime();

  const jobsLeftCount = jobCounts.waiting + jobCounts.delayed + jobCounts.active;

  if(jobsLeftCount === 0 && alreadyGotLastJob === false) {
    alreadyGotLastJob = true;
    const e = 'LAST JOB! queue completed no jobs left "fetch-user"';

    console.log(`${e} ${t} ${jobId} ${username}`);
    io.emit(e, `${t} ${jobId} ${username}`);
  } else {
    const e = 'queue completed job(s) left';

    console.log(`${e} ${t} ${jobId} ${username} ${jobsLeftCount}`);
    io.emit(e, `${t} ${jobId} ${username} ${jobsLeftCount}`);
  }
});

server.listen(PORT, () => {
  console.log(`VeryLastJob listening on port ${PORT}`);
});
